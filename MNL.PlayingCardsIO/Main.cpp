#include <iostream>
#include <conio.h>


using namespace std;

enum Rank
{
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	Six = 6,
	Seven = 7,
	Eight = 8,
	Nine = 9,
	Ten = 10,
	Jack = 11,
	Queen = 12,
	King = 13,
	Ace = 14
};

enum Suit
{
	Clubs = 1,
	Hearts = 3,
	Spades = 4,
	Diamonds = 2
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) {
	//cout << "The " << card.rank << " of " << card.suit;
	// the top code results in their id number being written

	cout << "The ";
		
	switch (card.rank)
		{
		case 2: cout << "Two";
			break;
		case 3: cout << "Three";
			break;
		case 4: cout << "Four";
			break;
		case 5: cout << "Five";
			break;
		case 6: cout << "Six";
			break;
		case 7: cout << "Seven";
			break;
		case 8: cout << "Eight";
			break;
		case 9: cout << "Nine";
			break;
		case 10: cout << "Ten";
			break;
		case 11: cout << "Jack";
			break;
		case 12: cout << "Queen";
			break;
		case 13: cout << "King";
			break;
		case 14: cout << "Ace";
			break;
		}

	cout << " of ";

	switch (card.suit)
	{
	case 1: cout << "Clubs";
		break;
	case 3: cout << "Hearts";
		break;
	case 4: cout << "Spades";
		break;
	case 2: cout << "Diamonds";
		break;
	}

	cout << "\n";


}

Card HighCard(Card card1, Card card2) {
	if (card1.rank == card2.rank) {
		if (card1.suit > card2.suit)
			return card1;
		else
			return card2;
	}
	else if (card1.rank > card2.rank)
		return card1;
	else
		return card2;
}

int main() {
	Card c1;
	c1.rank = Nine;
	c1.suit = Clubs;

	Card c2;
	c2.rank = Nine;
	c2.suit = Spades;

	PrintCard(c1);
	PrintCard(c2);

	Card High = HighCard(c1, c2);
	



	cout << "Highest card is: ";
	PrintCard(High);


	_getch();
	return 0;
}